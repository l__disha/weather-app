const key = "81304f9283b938b8f2ccae61f3034d5c";

const getForecast = async (city) => {
	const base = "http://api.openweathermap.org/data/2.5/forecast";
	const query = `?q=${city}&units=metric&appid=${key}`;
	const response = await fetch(base + query);
	if (response.ok) {
		const data = await response.json();
		return data;
	}
	throw new Error("Status Code: " + response.status);
}

getForecast("mumbai")
	.then(data => console.log(data))
	.catch(err => console.error(err));
